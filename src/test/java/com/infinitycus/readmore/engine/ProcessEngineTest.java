package com.infinitycus.readmore.engine;

import com.infinitycus.readmore.model.entity.Book;
import com.infinitycus.readmore.model.entity.ReadingProcess;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class ProcessEngineTest {

    private final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

    @Test
    public void testStartReadingProcessByMinutes() throws Exception {
        ReadingProcess readingProcess = new ReadingProcess();
        readingProcess.setBook(getBook());
        readingProcess.setStartDate(sdf.parse("04.01.2015"));
        readingProcess.setMinutesPerDay(40);
        readingProcess.setPagesPerMinute(1.5);
        ProcessEngine.startReadingProcess(readingProcess);
        Date finishDate = readingProcess.getCalculatedFinishDate();
        assertEquals("09.01.2015", sdf.format(finishDate));
    }

    @Test
    public void testStartReadingProcessByPages() throws Exception {
        ReadingProcess readingProcess = new ReadingProcess();
        readingProcess.setBook(getBook());
        readingProcess.setStartDate(sdf.parse("04.01.2015"));
        readingProcess.setPagesPerDay(30);
        ProcessEngine.startReadingProcess(readingProcess);
        Date finishDate = readingProcess.getCalculatedFinishDate();
        assertEquals("14.01.2015", sdf.format(finishDate));
    }

    private Book getBook() {
        Book book = new Book();
        book.setAuthor("Алан Купер");
        book.setName("Психбольница в руках пациентов");
        book.setNumberOfPages(300);
        return book;
    }
}