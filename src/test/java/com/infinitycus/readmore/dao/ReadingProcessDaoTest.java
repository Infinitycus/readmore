package com.infinitycus.readmore.dao;

import com.infinitycus.readmore.model.entity.Book;
import com.infinitycus.readmore.model.entity.ReadingProcess;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ReadingProcessDaoTest {

    private static ReadingProcessDao dao = new ReadingProcessDao();
    private String id;

    @Before
    public void setUp() throws Exception {
        this.id = insert().getId().toString();
    }

    @After
    public void tearDown() throws Exception {
        dao.deleteById(this.id);
    }

    @Test
    public void testInsert() throws Exception {
        assertNotNull(dao.findById(this.id));
    }

    @Test
    public void testFindById() throws Exception {
        assertNotNull(dao.findById(this.id));
    }

    @Test
    public void testFindAll() throws Exception {
        assertTrue(dao.findAll().size() > 1);
    }

    private ReadingProcess insert() {
        ReadingProcess readingProcess = new ReadingProcess();
        readingProcess.setBook(getBook());
        readingProcess.setStartDate(new Date());
        readingProcess.setMinutesPerDay(40);
        readingProcess.setPagesPerMinute(1.5);
        return dao.insert(readingProcess);
    }

    private Book getBook() {
        Book book = new Book();
        book.setAuthor("Алан Купер");
        book.setName("Психбольница в руках пациентов");
        book.setNumberOfPages(300);
        return book;
    }
}