package com.infinitycus.readmore.engine;

import com.infinitycus.readmore.model.entity.ReadingProcess;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author Evgeniy Myslovets
 * @date 04.01.2015
 */
public class ProcessEngine {

    public static void startReadingProcess(ReadingProcess readingProcess) {
        if (readingProcess.getMinutesPerDay() != null && readingProcess.getPagesPerMinute() != null) {
            startProcessByMinutes(readingProcess);
            return;
        }
        if (readingProcess.getPagesPerDay() != null) {
            startProcessByPages(readingProcess);
            return;
        }

        System.err.println("Not enough parameters to start reading process");
    }

    private static void startProcessByPages(ReadingProcess readingProcess) {
        Integer totalPages = readingProcess.getTotalPages();
        Integer pagesPerDay = readingProcess.getPagesPerDay();
        readingProcess.setCalculatedFinishDate(calculateFinishDate(totalPages, pagesPerDay));
    }

    private static void startProcessByMinutes(ReadingProcess readingProcess) {
        Integer totalPages = readingProcess.getTotalPages();
        Double calculatedPagesPerDay = readingProcess.getPagesPerMinute() * readingProcess.getMinutesPerDay();
        int pagesPerDay = (int) Math.round(Math.ceil(calculatedPagesPerDay));
        readingProcess.setPagesPerDay(pagesPerDay);
        readingProcess.setCalculatedFinishDate(calculateFinishDate(totalPages, pagesPerDay));
    }

    private static Date calculateFinishDate(Integer totalPages, Integer pagesPerDay) {
        Integer totalDays = (int) Math.round(Math.ceil((double) totalPages / pagesPerDay));
        LocalDate calculatedFinishDate = LocalDate.now().plusDays(totalDays);
        return Date.from(calculatedFinishDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }
}
