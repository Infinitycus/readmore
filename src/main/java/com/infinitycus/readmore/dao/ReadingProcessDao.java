package com.infinitycus.readmore.dao;

import com.infinitycus.readmore.model.entity.ReadingProcess;
import com.infinitycus.readmore.service.config.DataSourceUtil;
import org.bson.types.ObjectId;
import org.mongodb.morphia.dao.BasicDAO;

import java.util.List;

/**
 * @author Evgeniy Myslovets
 * @date 04.01.2015
 */
public class ReadingProcessDao extends BasicDAO<ReadingProcess, ObjectId> {

    public ReadingProcessDao() {
        super(DataSourceUtil.getConnection(), DataSourceUtil.getODMImpl(), DataSourceUtil.DB_NAME);
    }

    public ReadingProcess insert(ReadingProcess readingProcess) {
        save(readingProcess);
        return readingProcess;
    }

    public List<ReadingProcess> findAll() {
        return createQuery().asList();
    }

    public ReadingProcess findById(String id) {
        return findOne("_id", new ObjectId(id));
    }

    public void deleteById(String id) {
        deleteById(new ObjectId(id));
    }
}
