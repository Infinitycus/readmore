package com.infinitycus.readmore.model.entity;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.Date;

/**
 * @author Evgeniy Myslovets
 * @date 04.01.2015
 */
@Entity(noClassnameStored = true)
public class ReadingProcess {

    @Id
    private ObjectId id;
    private Book book;
    private Integer minutesPerDay;
    private Double pagesPerMinute;
    private Integer pagesPerDay;
    private Integer currentPage;
    private Date startDate;
    private Date calculatedFinishDate;
    private Date actualFinishDate;

    public ReadingProcess() {
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Integer getTotalPages() {
        return this.book.getNumberOfPages();
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Integer getMinutesPerDay() {
        return minutesPerDay;
    }

    public void setMinutesPerDay(Integer minutesPerDay) {
        this.minutesPerDay = minutesPerDay;
    }

    public Double getPagesPerMinute() {
        return pagesPerMinute;
    }

    public void setPagesPerMinute(Double pagesPerMinute) {
        this.pagesPerMinute = pagesPerMinute;
    }

    public Integer getPagesPerDay() {
        return pagesPerDay;
    }

    public void setPagesPerDay(Integer pagesPerDay) {
        this.pagesPerDay = pagesPerDay;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getCalculatedFinishDate() {
        return calculatedFinishDate;
    }

    public void setCalculatedFinishDate(Date calculatedFinishDate) {
        this.calculatedFinishDate = calculatedFinishDate;
    }

    public Date getActualFinishDate() {
        return actualFinishDate;
    }

    public void setActualFinishDate(Date actualFinishDate) {
        this.actualFinishDate = actualFinishDate;
    }
}
