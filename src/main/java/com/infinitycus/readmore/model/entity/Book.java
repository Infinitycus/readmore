package com.infinitycus.readmore.model.entity;

/**
 * @author Evgeniy Myslovets
 * @date 04.01.2015
 */
public class Book {

    private String name;
    private String author;
    private Integer numberOfPages;

    public Book() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }
}
