package com.infinitycus.readmore.service.config;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.mongodb.*;
import org.apache.commons.lang3.StringUtils;
import org.mongodb.morphia.Morphia;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;

/**
 * @author Evgeniy Myslovets
 * @date 04.01.2015
 */
public class DataSourceUtil {

    public static final String DB_NAME = "readmore";

    private static MongoConfiguration mongoConfiguration;
    private static Morphia morphia;
    private static MongoClient mongoClient;

    public static synchronized Morphia getODMImpl() {
        if (morphia == null) {
            morphia = new Morphia();
            morphia.mapPackage("com.infinitycus.readmore.model.entity");
        }
        return morphia;
    }

    public static synchronized MongoConfiguration getMongoConfiguration() {
        if (mongoConfiguration == null) {
            mongoConfiguration = loadMongoConfiguration();
        }
        return mongoConfiguration;
    }

    public static synchronized MongoClient getConnection() {
        if (mongoClient == null) {
            mongoClient = new MongoClient(
                    getMongoConfiguration().getServerAddress(),
                    new MongoClientOptions.Builder().build());
            DB db = mongoClient.getDB(DB_NAME);
            MongoCredential credential = getMongoConfiguration().getMongoCredential();
            if (db.authenticate(credential.getUserName(), credential.getPassword())) {
                String message = String.format("User is not authenticated to access database %s", db.getName());
                throw new RuntimeException(message);
            }
        }
        return mongoClient;
    }

    private static MongoConfiguration loadMongoConfiguration() {
        String filePath = DB_NAME + "-" + System.getProperty("env", "LOCAL") + "-database.json";
        try (InputStream inStream = DataSourceUtil.class.getResourceAsStream("/" + filePath);
             JsonReader reader = new JsonReader(new InputStreamReader(inStream))) {
            return new Gson().<MongoConfiguration>fromJson(reader, MongoConfiguration.class).build();
        } catch (Exception e) {
            throw new RuntimeException("Error while reading database properties: " + e.getMessage(), e);
        }
    }

    public static class MongoConfiguration {

        private ServerAddress serverAddress;
        private MongoCredential mongoCredential;

        private Address address;
        private Credential credential;

        public MongoConfiguration build() throws ConfigurationException {
            initAddress(getAddress());
            initCredentials(getCredential());
            return this;
        }

        public ServerAddress getServerAddress() {
            return serverAddress;
        }

        public MongoCredential getMongoCredential() {
            return mongoCredential;
        }

        private void initAddress(Address address) throws ConfigurationException {
            String host = address.getHost() == null ? ServerAddress.defaultHost() : address.getHost();
            int port = address.getPort() == null ? DBPort.PORT : Integer.valueOf(address.getPort());
            try {
                this.serverAddress = new ServerAddress(host, port);
            } catch (UnknownHostException e) {
                String message = String.format("Host %s:%s is not accessible, error: %s", host, port, e.getMessage());
                throw new ConfigurationException(message, e);
            }
        }

        private void initCredentials(Credential credential) throws ConfigurationException {
            String username = credential.getUsername();
            String password = credential.getPassword();
            String dbName = credential.getDbName();
            if (StringUtils.isEmpty(username)) {
                throw new ConfigurationException("Username can not be a null");
            }
            if (StringUtils.isEmpty(password)) {
                throw new ConfigurationException("password can not be a null");
            }
            if (StringUtils.isEmpty(dbName)) {
                throw new ConfigurationException("dbname can not be a null");
            }
            this.mongoCredential = MongoCredential.createMongoCRCredential(username, dbName, password.toCharArray());
        }

        public Address getAddress() {
            return address;
        }

        public void setAddress(Address address) {
            this.address = address;
        }

        public Credential getCredential() {
            return credential;
        }

        public void setCredential(Credential credential) {
            this.credential = credential;
        }

        public static class Address {
            private String host;
            private String port;

            public String getHost() {
                return host;
            }

            public void setHost(String host) {
                this.host = host;
            }

            public String getPort() {
                return port;
            }

            public void setPort(String port) {
                this.port = port;
            }
        }

        public static class Credential {
            private String username;
            private String password;
            private String dbName;

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public String getDbName() {
                return dbName;
            }

            public void setDbName(String dbName) {
                this.dbName = dbName;
            }
        }
    }
}
