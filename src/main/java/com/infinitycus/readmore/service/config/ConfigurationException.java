package com.infinitycus.readmore.service.config;

/**
 * @author Evgeniy Myslovets
 * @date 04.01.2015
 */
public class ConfigurationException extends Exception {

    public ConfigurationException(String message, Exception e) {
        super(message, e);
    }

    public ConfigurationException(String message) {
        super(message);
    }
}
