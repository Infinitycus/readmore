package com.infinitycus.readmore.service;

import com.infinitycus.readmore.dao.ReadingProcessDao;
import com.infinitycus.readmore.engine.ProcessEngine;
import com.infinitycus.readmore.model.entity.ReadingProcess;

import java.util.List;

/**
 * @author Evgeniy Myslovets
 * @date 04.01.2015
 */
public class ReadingProcessApi {

    private static ReadingProcessApi readingProcessApi;
    private ReadingProcessDao readingProcessDao;

    public ReadingProcessApi(ReadingProcessDao readingProcessDao) {
        this.readingProcessDao = readingProcessDao;
    }

    public static synchronized ReadingProcessApi getInstance() {
        if (readingProcessApi == null) {
            readingProcessApi = new ReadingProcessApi(new ReadingProcessDao());
        }
        return readingProcessApi;
    }

    public ReadingProcess startReading(ReadingProcess readingProcess) {
        ProcessEngine.startReadingProcess(readingProcess);
        return readingProcessDao.insert(readingProcess);
    }

    public List<ReadingProcess> findAll() {
        return readingProcessDao.findAll();
    }

    public ReadingProcess findById(String id) {
        return this.readingProcessDao.findById(id);
    }

    public void deleteById(String id) {
        this.readingProcessDao.deleteById(id);
    }
}
